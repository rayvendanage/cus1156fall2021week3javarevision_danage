import java.util.ArrayList;

/**
 * This class represents a group of cats. The cats are created when the CatManager is created
 * although more cats can be added later.
 * 
 */
public class CatManager 
{
	private ArrayList<Cat>  myCats;
	

	public CatManager()
	{
		myCats = new  ArrayList <Cat>();
		Cat cat1 = new Cat("Fifi", "black");
		myCats.add(cat1);
		Cat cat2 = new Cat("Fluffy", "spotted");
		myCats.add(cat2);
		Cat cat3 = new Cat("Josephine", "tabby");
		myCats.add(cat3);
		Cat cat4 = new Cat("Biff", "tabby");
		myCats.add(cat4);
		Cat cat5 = new Cat("Bumpkin", "white");
		myCats.add(cat5);
		Cat cat6 = new Cat("Spot", "spotted");
		myCats.add(cat6);
		Cat cat7 = new Cat("Lulu", "tabby");
		myCats.add(cat7);
		System.out.println("Done!");
	}

	/** add a cat to the group
	 * 
	 * @param name
	 * @return
	 */
	public void add(Cat aCat)
	{
		myCats.add(aCat);
	}


	/**returns the first cat whose name matches, or null if no cat
                    // with that name is found
	 * 
	 * @param name - the name of the Cat that we are searching for
	 * @return the Cat object or null if not found
	 */
	public Cat findThisCat(String name)
	{
		for(int i = 0; i<myCats.size(); i++) {
			if(myCats.get(i).getName().contentEquals(name)) {
				return myCats.get(i);
			}
		}
		return null;
	}

	// returns the number of cats of this color
	/**
	 * returns the number of cats of this color
	 * @param color - the color we are counting
	 * @return - the number of cats
	 */
	public int countColors(String color)
	{           
		int colorcounter = 0;
		for(int i = 0; i<myCats.size(); i++) {
			if(myCats.get(i).getColor() == color) {
				colorcounter++;
			}
		}
		return colorcounter;

	}
}
